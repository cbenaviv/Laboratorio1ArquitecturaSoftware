/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autosconcesionario.ejb;

import com.autosconcesionario.entity.Vehiculos;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author W
 */
@Stateless
public class VehiculosFacade extends AbstractFacade<Vehiculos> implements VehiculosFacadeLocal {

    @PersistenceContext(unitName = "AutosConcesionarioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VehiculosFacade() {
        super(Vehiculos.class);
    }

    @Override
    public void consultarVehiculo(String matricula) {
        Query q = em.createQuery("SELECT a FROM Vehiculos a where a.vin=:m");
        q.setParameter("m", matricula);
    }

    @Override
    public Vehiculos consultaVehiculo(int matricula) {
        Query q = em.createQuery("SELECT a FROM Vehiculos a where a.vin=:m");
        q.setParameter("m", matricula);
        Vehiculos vehiculoMatricula = (Vehiculos) q.getResultList().get(0);
        return vehiculoMatricula;
    }

    @Override
    public List<String> obtenerFabricantes() {

        Query q = em.createQuery("SELECT a.fabricante FROM Vehiculos a GROUP BY a.fabricante");
        List<String> listaFabricante;
        listaFabricante = q.getResultList();

        return listaFabricante;
    }

    @Override
    public String primerFabricante() {

        Query q = em.createQuery("SELECT a.fabricante FROM Vehiculos a GROUP BY a.fabricante");
        String listaFabricante;
        listaFabricante = (String) q.getResultList().get(0);

        return listaFabricante;
    }

    @Override
    public List<String> obtenerColorPorModelo(String modelo) {
        List<String> colorModelo;
        Query q = em.createQuery("SELECT a.color FROM Vehiculos a WHERE a.modelo=:m GROUP BY a.color ");
        q.setParameter("m", modelo);
        colorModelo = (List<String>) q.getResultList();

        return colorModelo;
    }

    @Override
    public List<Vehiculos> obtenerVehiculosComprados() {
        List<Vehiculos> lista;
        Query q=em.createQuery("SELECT  a FROM Vehiculos a JOIN a.ventasgeneralesList p");
        
        lista=q.getResultList();
        
        return lista;
        
    }
    
    @Override
    
    public List<String> vehiculosPorModelos(String marca)
    {
        List<String>listaModelo;
        Query q=em.createQuery("SELECT DISTINCT(a.modelo) FROM Vehiculos a where a.fabricante=:m");
        q.setParameter("m", marca);
        listaModelo=q.getResultList();
        return listaModelo;
    }
    
    @Override
    
    public List<String> vehiculosPorMarcaMode(String marca,String modelo)
    {
        List<String>listColores;
        Query q=em.createQuery("SELECT DISTINCT(a.color) FROM Vehiculos a where a.fabricante=:f and a.modelo=:m");
        q.setParameter("f", marca);
        q.setParameter("m", modelo);
        listColores=q.getResultList();
        return listColores;
        
        
    }
    
    
    

}
