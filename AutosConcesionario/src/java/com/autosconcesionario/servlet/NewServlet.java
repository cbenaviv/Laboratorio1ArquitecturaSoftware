/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.autosconcesionario.servlet;

import com.autosconcesionario.ejb.ClientesFacadeLocal;
import com.autosconcesionario.ejb.VehiculosFacadeLocal;
import com.autosconcesionario.ejb.VentasgeneralesFacadeLocal;
import com.autosconcesionario.entity.Clientes;
import com.autosconcesionario.entity.Vehiculos;
import com.autosconcesionario.entity.Ventasgenerales;
import com.sun.mail.iap.ByteArray;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import static java.lang.System.out;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.ejb.EJB;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Cesar Benavidez
 */
public class NewServlet extends HttpServlet {

    @EJB
    private VentasgeneralesFacadeLocal ventasgeneralesFacade;
    @EJB
    private VehiculosFacadeLocal vehiculoFacade;
    @EJB

    private ClientesFacadeLocal clientesFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()) {

            String action = request.getParameter("action");
            String url = "clientes.jsp";

            if ("Clientes".equals(action)) {
                url = "clientes.jsp";
                Clientes cliente = new Clientes();
                cliente.setDocumento(request.getParameter("documento"));
                cliente.setNombres(request.getParameter("nombres"));
                cliente.setApellidos(request.getParameter("apellidos"));
                cliente.setCorreo(request.getParameter("correo"));
                cliente.setTelefono(request.getParameter("telefono"));
                clientesFacade.create(cliente);

            } else if ("vehiculos".equals(action)) {

                String a=request.getParameter("vin");
                String b=request.getParameter("modelo");
                String c=request.getParameter("color");
                String d=request.getParameter("fabricante");
                String e=request.getParameter("picture");
                String f=request.getParameter("precio");
                
                
                Vehiculos vehiculo = new Vehiculos();
                vehiculo.setVin(a);
                vehiculo.setModelo(b);
                vehiculo.setColor(c);
                vehiculo.setFabricante(e);
                vehiculo.setPrecio(Float.parseFloat(f));
                try
                    
                {
                  BufferedImage bufferedImage=ImageIO.read(request.getPart("picture").getInputStream());
                ByteArrayOutputStream baos=new ByteArrayOutputStream();
                ImageIO.write(bufferedImage,"jpg",baos);
                baos.flush();
                
                byte[]image=baos.toByteArray();
                    
                }
                catch(Exception exception){
                    System.out.println(exception);
                }
                
                
               
               url = "vehiculos.jsp";
                //int test=0;
             

            } else if ("listarVehiculos".equals(action)) {
                url = "listaVehiculos.jsp";
                List<Vehiculos> findAll = vehiculoFacade.findAll();
                request.getSession().setAttribute("vehiculos", findAll);

            } else if ("listarUnVehiculo".equals(action)) {

                Vehiculos v = vehiculoFacade.find("9495");
                url = "listarVehiculos.jsp";
            } else if ("listarClientes".equals(action)) {
                url = "listaVehiculos.jsp";
                List<Clientes> findAll = clientesFacade.findAll();
                request.getSession().setAttribute("clientes", findAll);
            } else if ("ListarUnCliente".equals(action)) {

                Clientes c = clientesFacade.find("1075289720");
                url = "listaVehiculos.jsp";
            } else if ("ListarCarrosDisponibles".equals(action)) {
                List<Vehiculos> listaCarrosTotal = vehiculoFacade.findAll();///Estan todo los carros
                List<Vehiculos> listaCarrosVendidos = vehiculoFacade.obtenerVehiculosComprados();
                ArrayList<Vehiculos> listaCorrecta = new ArrayList<>();

                for (int i = 0; i < listaCarrosTotal.size(); i++) {
                    int contador = 0;

                    for (int j = 0; j < listaCarrosVendidos.size(); j++) {

                        String ct = listaCarrosTotal.get(i).getVin();
                        String cv = listaCarrosVendidos.get(j).getVin();

                        if (ct.equals(cv)) {
                            contador++;
                        }

                    }

                    if (contador == 0) {
                        listaCorrecta.add(listaCarrosTotal.get(i));
                    }

                }

                url = "listaVehiculos.jsp";

            } else if ("ListarFabricanteModelos".equals(action)) {
                List<String> modelos = vehiculoFacade.vehiculosPorModelos("Renault");
                url = "listaVehiculos.jsp";

            } else if ("ListarFabricantesModeloColor".equals(action)) {
                List<String> color = vehiculoFacade.vehiculosPorMarcaMode("Renault", "2007");
                url = "listaVehiculos.jsp";
            } else if ("GenerarVenta".equals(action));
            {
                Ventasgenerales venta = new Ventasgenerales();
                Clientes cliente = new Clientes();
                cliente.setDocumento("1075289720");
                Vehiculos vehiculo = new Vehiculos();
                vehiculo.setFabricante("");
                vehiculo.setModelo("");
                vehiculo.setColor("");
                vehiculo.setPrecio(94949);
                venta.setClienteDocumento(cliente);
                venta.setCodigoVenta(1);
                String fecha = "2017-02-22";
                DateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                Date fechaDate = null;

                try {
                    fechaDate = formato.parse(fecha);

                } catch (ParseException ex) {
                    System.out.println(ex);
                }

                venta.setFechaVenta(fechaDate);
                venta.setVehiculoCodigo(vehiculo);
                ventasgeneralesFacade.create(venta);

            }

            response.sendRedirect(url);
        } finally {
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
